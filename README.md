## Installation

App requires [Node.js](https://nodejs.org/) v14+ to run.

Install the dependencies and devDependencies and start the server.

```sh
cd course-fee-structure
yarn install
yarn start
```

To run in android

```sh
yarn run android
```

To run in ios

```sh
cd ios && pod install && cd ..
yarn run ios
```
